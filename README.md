## About

Source for the websites:

- https://www.teamobsession.at
- https://freerunningacademy.at

## Usage

Deployed to [GitLab Pages](https://gitlab.com/team-obsession/teamobsession.at/pages) with configured custom domains.

- Domain owner: @dnsmichi
- DNS admin: @lazyfrosch

## Updates

0. Request access at the project to become a `maintainer`. Ping @dnsmichi to approve. 
1. Follow the [development documentation](#development) to make changes and preview them.
2. Create a new Git branch, commit the changes, push them to the main branch.
4. The GitLab Pages deployment from the `main` branch is triggered automatically, and deploys the new content to the website.

## Development

### GitLab Web IDE 

Open the Web IDE from the project overview. It will open in a new tab. _Note that the Web IDE does not provide a website preview yet, and is suggested for quick changes/updates._

![Project overview Web IDE](docs/images/project_overview_webide.png)

Edit the files, selected from the explorer tree. On the left menu, you'll recognize the source control icon with a blue bubble and a number. This tracks the changes made, and is the next step to commit and sync the changes with the Git repository.

![Web IDE edit](docs/images/web_ide_edit.png)

Navigate into the `Source Control` menu, and prepare to commit the changes. Provide a short title summary what these changes are for, e.g. `Add new summer event info` and press the button to commit & push.

![Source control commit](docs/images/web_ide_source_control_commit_message.png)

The Web IDE asks you to push to a new branch, or the default `main` branch. Use the cursor to select the `main` branch, and press `enter`. At the bottom right, the Web IDE tells you that it persisted the changes, and provides `Go to project` to open a new tab to verify the Git repository.

![Commit changes, main branch](docs/images/web_ide_commit_changes_main_branch.png)

Once the changes are pushed, this automatically kicks off the CI/CD pipelines which will deploy the website content to GitLab Pages - the webserver that serves the website, reachable at https://teamobsession.at 

You can watch the CI/CD pipelines progress [here](https://gitlab.com/team-obsession/teamobsession.at/-/pipelines).


### Gitpod

Open https://gitpod.io/#https://gitlab.com/team-obsession/teamobsession.at with permissions to the source project on GitLab.com 

When asked by Gitpod, login using GitLab oauth.

To sync changes, Gitpod requires the GitLab workflow extension to be installed and configured with a Personal Access Token. The reason is that these are separate systems that do not trust each other. You can follow the steps in [this blog post](https://about.gitlab.com/blog/2021/07/19/teams-gitpod-integration-gitlab-speed-up-development/#vs-code-extensions).

### Local

1. Install the dependencies for a local development environment (i.e. Git, NodeJS and VS Code)
2. Create a [personal access token with `write_repository` scope](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) (HTTPS sync) 
3. Clone the Git repository locally using the Git CLI
4. Change into the directory, make changes, commit
5. Push to the main branch 


Example on a terminal on macOS:

```
brew install git node 

git clone https://gitlab.com/team-obsession/teamobsession.at
cd teamobsession.at 
```

```shell 
# Install dependencies
npm install 

# Start a local web server 
npx --yes serve
```

Open a new terminal, or your preferred editor (VS Code) and navigate into the file tree. 

```shell
# edit 
vim index.html

# commit the change
git commit -av -m "Add new summer event"

# push the change to the remote git repository that triggers the website deployment 
git push 
```

When pushing the changes, the Git CLI or VS Code will ask for a username and password.

- Username: your username
- Password: your personal access token 


#### Rebuild CSS 

```shell
npx --yes browserslist@latest --update-db 
npm run build-css
```

#### Local webserver 

Run the webserver, starting on http://localhost:3000

```shell
npx --yes serve
```

## Thanks

[Tobias Huch](https://github.com/Tobias-Huch) for creating this website. 

